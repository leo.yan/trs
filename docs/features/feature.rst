########
Features
########

Secure Boot
***********

The firmware component of TRS unconditionally enables UEFI secure boot for all
supported platforms.  You can find more information :ref:`here <Build and install>`

Measured Boot
*************
TRS is designed to take advantage of Trusted Platform modules.  The firmware
part of TRS supports the `EFI TCG Protocol
<https://trustedcomputinggroup.org/resource/tcg-efi-protocol-specification/>`_
as well as `TCG PC Client Specific Platform Firmware Profile Specification 
<https://trustedcomputinggroup.org/resource/pc-client-specific-platform-firmware-profile-specification/>`_
and provides the building blocks the OS needs for measured boot.

During the OS first boot it will automatically scan for a TPM.  If such a
device is present it will generate a random key, encrypt the root filesystem
and seal it against measurements found in `PCR7` which holds the Secure Boot
Policy and EFI keys used for UEFI Secure Boot.

.. _op-tee-os:

OP-TEE OS
*********
OP-TEE is our Secure World OS of choice in TRS.  We use it for a number of
reasons with the most notable ones being 

- Run [fTPM]_ is the hardware doesn't have a discrete TPM.
- Store EFI variables on boards that have an RPMB.
- Provide a DRBG if the hardware doesn't provide a TRNG.
- Provide a PKCS#11 provider to PARSEC.

Conceptually the components interacting with OP-TEE in the TRS build can be seen
in the image below. The ``Features`` lane there indicates which exceptions
levels are involved in a certain use case. For example, "TEE: Secure Storage"
is all kept in (S)EL-0 and (S)EL-1.

.. image:: ../images/optee-trs.png
     :alt: OP-TEE TRS component overview

Note that this image is rather generic as depicted here. We have other areas
that could (and should) be added as well, for example ``SCMI``, ``Xen``,
``FF-A``, ``SwTPM`` to name a few. But perhaps it's better to add them as
separate diagrams to avoid making the images too complex.
