.. _install-qemu:

Install QEMU
############

This document describes how to build and run for QEMU target and we suppose that
you have been already fulfilled the steps mentioned at the
:ref:`getting_started` page. If not, start there before continuing.

Building
********
Since we're using a virtual python enviroment, start by source that

.. code-block:: bash

    $ source <workspace root>/.pyvenv/bin/activate

.. note::
   The ``source`` above has to be done **once** everytime you spawn a new shell.

Start the build, this will probably take several hours on a normal desktop
computer the first time you're building it with nothing in the cache(s). The TRS
is based on various Yocto layers and if you don't have your ``DL_DIR`` and
``SSTATE_DIR`` set as an environment variable, those will be set to
``$HOME/yocto_cache`` by default. Note that the clean target will leave
download and sstate chaches in place and is relatively fast operation
and quite often needed when updates have been applied to the yocto meta layers.

.. code-block:: bash

    $ cd <workspace root>
    $ make clean && make all

This will build firmware (meta-ts) for all target machines and
generic kernel, initramfs and rootfs. Alternatively, they can be built
separately.


.. code-block:: bash

    $ cd <workspace root>
    $ make clean && make meta-ts && make trs

Run
***
Once the build has been completed, you can run it on your host machine with
QEMU.

.. code-block:: bash

    $ cd <workspace root>
    $ make run

Already at first boot, u-boot is configured to boot the corrent kernel, initramfs and rootfs. On other systems
the configuration below needs to be applied manually.

.. code-block:: bash

    => efidebug boot add -b 1 BootLedge virtio 0:1 efi/boot/bootaa64.efi -i virtio 0:1 ledge-initramfs.rootfs.cpio.gz -s 'console=ttyAMA0,115200 console=tty0 root=UUID=6091b3a4-ce08-3020-93a6-f755a22ef03b rootwait panic=60' ; efidebug boot order 1 ; bootefi bootmgr

.. note::
    To quit QEMU, press ``Ctrl-A x`` (alternatively kill the qemu-system-aarch64 process)

If everything goes as expected, you will presented login message and a login
prompt. The login name is ``ewaol`` as depicted below.

.. code-block:: bash

    ledge-secure-qemuarm64 login: ewaol
    ewaol@ledge-secure-qemuarm64:~$

Test
****
Once the build has been completed, you can run automatic tests with QEMU. These boot
QEMU using the compiled images and run test commands via SSH on the running system.
While the QEMU image is running, SSH access to it works via localhost IP address
127.0.0.1 and TCP port 2222. TEST_SUITES variable in trs-image.bb recipe define
which tests are executed.

.. code-block:: bash

    $ cd <workspace root>
    $ make test

FAQ
***

..
  [NEEDS_TO_BE_FIXED] - Empty section, see Rockpi4 as an example

TBD
