.. _install-rockpi4:

Instll RockPi4
##############

.. warning::
   Although the instructions below should work, they so far doesn't leverage the
   "TRS" makefiles etc.

This document describes how to build and run for RockPi4b+ target.

Building RockPi4
****************

Build meta-ts
=============

Refer to the firmware :ref:`documentation <Build and install>` section for
details

Build ewaol plus ledge security rootfs image
================================================
.. code-block:: bash

	$ mkdir trs
	$ cd trs
	$ git clone https://gitlab.com/Linaro/ewaol/meta-ewaol-machine.git -b kirkstone-dev
	$ cd meta-ewaol-machine
	$ ./build.sh ledge-secure-qemuarm64 baremetal

	# Note: the rootfs is common with other arm64 targets, so the same .wic image can be used

Run
***
How to prepare the images to run on the RockPi 4 board is explained here,
copied/annotated below:

Firmware boots from an SD card. While rootfs and ESP partition are on a USB
stick. This allows the USB stick to be easily used in multiple boards.

Prepare SD with
===============

Refer to trusted substrate docs for more information
https://trusted-substrate.readthedocs.io/en/latest/building/install_firmware.html

Assuming your SD card is /dev/sda

.. code-block:: bash

	$ cp meta-ts/build/tmp/deploy/images/rockpi4b/ts-firmware-rockpi4b.wic.gz .
	$ zcat ts-firmware-rockpi4b.wic.gz >/dev/sda

.. warning::

	Make sure the device in ``/dev`` is present before doing this or you will
	create a file in ``/dev`` and put nothing on the SD card, and the card will
	not be seen until you delete the file!

Prepare USB stick with
======================

To flash the rootfs image you built above, from your trs build directory

.. code-block:: bash

	$ sudo dd if=build/ledge-secure-qemuarm64/tmp_baremetal/deploy/images/ledge-secure-qemuarm64/ewaol-baremetal-image-ledge-secure-qemuarm64.wic of=/dev/sda bs=1M status=progress
	$ sync

.. note::

	The ``qemuarm64`` above not a typo, the rootfs is multi-platform. If you
	used USB stick in other machine with current firmware before booting delete
	``ubootefi.var`` file for ESP (first one) partition.

Attach USB stick and SD card
============================

Plug both USB stick and SD card into the board. RockPi 4 provides four USB
ports: two black USB2 ports and two blue USB3 ports; to make sure you are using
a compatible USB port for the USB stick, you can power the board on and wait for
the U-boot prompt and use command `usb storage` to verify if the USB stick is
detected correctly:

.. code-block:: bash

        => usb start
        => usb storage
          Device 0: Vendor: SanDisk Rev: 1.00 Prod: Cruzer Blade
                    Type: Removable Hard Disk
                    Capacity: 29340.0 MB = 28.6 GB (60088320 x 512)

Add kernel board specific kernel parameters and EFI boot order
==============================================================

.. code-block:: bash

	$ efidebug boot add -b 1 BootLedge usb 0:1 efi/boot/bootaa64.efi -i usb 0:1 ledge-initramfs.rootfs.cpio.gz -s 'console=ttyS2,1500000 console=tty0 root=UUID=6091b3a4-ce08-3020-93a6-f755a22ef03b rootwait panic=60' ; efidebug boot order 1

Only attach SD card
===================

Alternatively we can use only SD card to boot system and the USB stick is not
required. In this case, we need to merge firmware and root file system images
into a single image and store it into the SD card, this can be finished with
below steps:

.. code-block:: bash

        $ gunzip ts-firmware-rockpi4b.wic.gz
        $ wget https://git.linaro.org/ci/job/configs.git/plain/ledge/ts/scripts/ts-merge-images.sh
        $ chmod +x ts-merge-images.sh
        $ ./ts-merge-images.sh ts-firmware-rockpi4b.wic ewaol-baremetal-image-ledge-secure-qemuarm64.wic

To boot from EFI partition in SD card, we need firstly get to know what's the
partition number for the EFI system partition:

.. code-block:: bash

        $ fdisk -l ts-firmware-rockpi4b.wic
        Disk ts-firmware-rockpi4b.wic: 2.28 GiB, 2443199488 bytes, 4771874 sectors
        Units: sectors of 1 * 512 = 512 bytes
        Sector size (logical/physical): 512 bytes / 512 bytes
        I/O size (minimum/optimal): 512 bytes / 512 bytes
        Disklabel type: gpt
        Disk identifier: B9476BE0-8456-4A3B-98D4-75A91739819F

        Device                     Start     End Sectors  Size Type
        ts-firmware-rockpi4b.wic1     64    8063    8000  3.9M unknown
        ts-firmware-rockpi4b.wic2   8064    8191     128   64K Microsoft basic data
        ts-firmware-rockpi4b.wic3   8192   16383    8192    4M Microsoft basic data
        ts-firmware-rockpi4b.wic4  16384   24575    8192    4M unknown
        ts-firmware-rockpi4b.wic5  24576   32767    8192    4M Microsoft basic data
        ts-firmware-rockpi4b.wic6  32768  557055  524288  256M EFI System
        ts-firmware-rockpi4b.wic7 557056 4751359 4194304    2G Linux filesystem

In this case the EFI system partiton number is 6 (it's ts-firmware-rockpi4b.wic6
so the partition number is ordered as 6) and SD card is mmc device 1, based on
these info we can decide the EFI system partition is device "mmc 1:6". Finally
we can create a boot entry with U-boot command `efidebug`:

.. code-block:: bash

        => mmc list
        mmc@fe310000: 2
        mmc@fe320000: 1 (SD)
        mmc@fe330000: 0
        => efidebug boot add -b 1 BootLedge mmc 1:6 efi/boot/bootaa64.efi -i mmc 1:6 ledge-initramfs.rootfs.cpio.gz -s 'console=ttyS2,1500000 console=tty0 root=UUID=6091b3a4-ce08-3020-93a6-f755a22ef03b rootwait panic=60' ; efidebug boot order 1

Restart
=======

Power cycle board it and it has to boot automatically now.

.. note::
	Second boot with a fresh root fs is quite slow, please wait for a couple
	of minutes. This is caused by the rootfs being encrypted on first boot.

Serial port info: https://wiki.radxa.com/Rockpi4/dev/serial-console. We've been
using this script:

.. code-block:: bash

	#!/bin/bash
	#
	# miniterm.py is in Ubuntu package python-serial
	# $ sudo apt-get install python-serial
	#
	# Adjust USB device as needed

	DEV=${1:-/dev/ttyUSB0}
	pyserial-miniterm --raw --eol CR ${DEV} 1500000


RockPi4 FAQ
***********

Q: How to increase OP-TEE core log level?
=========================================
Add ``CFG_TEE_CORE_LOG_LEVEL=3`` to ``EXTRA_OEMAKE`` in
``meta-ts/meta-arm/recipes-security/optee/optee-os.inc`` and rebuild (kas
build…)

Q: How to modify optee-os sources locally and rebuild?
======================================================

	#. Remove line ``INHERIT += rm_work`` in ``ci/base.yml``
	#. Run ``$ kas shell ci/rockpi4b.yml``
		#. ``bitbake -c cleansstate optee-os`` # WARNING removes source in work directory
		#. ``$ bitbake optee-os``
		#. Edit source files in ``build/tmp/work/rockpi4b-poky-linux/optee-os/<ver>/git``
		   ``$ bitbake -c compile -f optee-os`` # mandatory before kas build below it seems
	#. Exit kas shell and run ``$ kas build ci/rockpi4b.yml``

Q: Why is the internal eMMC not detected?
=========================================
Try a different USB-C power supply. We use a Dell one. I have another no-name PS
supposedly rated PD 100W which doesn’t work reliably.

Q: How to skip initramfs and boot to rootfs directly?
=====================================================

.. code-block:: bash

	$ efidebug boot add -b 1 BootLedge usb 0:1 efi/boot/bootaa64.efi -s 'console=ttyS2,1500000 console=tty0 root=UUID=6091b3a4-ce08-3020-93a6-f755a22ef03b rootwait panic=60 root=/dev/sda2' ; efidebug boot order 1 ; bootefi bootmgr

Q: On boot, the kernel logs warnings about GPT, how to fix them?
================================================================
They are harmless, they are caused by the fact that the actual device (USB key)
is larger than the image copied to it. The warnings can be removed by running
``gparted /dev/sdaX`` and accepting the prompt to fix the GPT info.

Q: On boot, the kernel logs “EXT4 … recovery complete”, what’s wrong?
=====================================================================
Usually harmless. The board was not powered off or rebooted cleanly. Use
``systemctl halt`` or ``systemctl reboot``.

Q: symbolize.py on hb2 (on e.g., the fTPM TA) prints DWARF warnings and no source file/line info. Why?
======================================================================================================
The default toolchains (``aarch64-linux-gnu-*``) is too old (7.2). Put a more
recent one in your ``PATH`` before invoking ``symbolize.py`` (Note: some source/file
line info are still missing, could be due to build flags)

Q: My board randomly hangs or crashes under system load. Why?
=====================================================================
RockPi4b boards are very fussy about their PSU. Ensure you are using an official
PSU like
https://shop.allnetchina.cn/products/power-supply-adapter-qc-3-0-for-rock-pi-4

Do not use a 5v only USB-C PSU (such as a USB port on your laptop), as you will
hit random board stability issues.
