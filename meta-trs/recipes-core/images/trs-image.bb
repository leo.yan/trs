# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

IMAGE_FSTYPES = "wic"

# based on EWAOL
require ${COREBASE}/../meta-ewaol/meta-ewaol-distro/recipes-core/images/ewaol-image-core.inc

SUMMARY = "TRS image"
LICENSE = "MIT"

SRC_URI = "file://testimage_data.json"
DEPENDS += "e2fsprogs-native"

inherit features_check deploy

# re-enable SRC_URI handling, it's disabled in image.bbclass
python __anonymous() {
    d.delVarFlag("do_fetch", "noexec")
    d.delVarFlag("do_unpack", "noexec")
}

REQUIRED_DISTRO_FEATURES = "ewaol-baremetal"
CONFLICT_DISTRO_FEATURES = "ewaol-virtualization"

IMAGE_FEATURES:append = " debug-tweaks"

# setup testimage.bbclass to execute oeqa runtime tests
# TODO: add systemd test once boot is clean
TEST_RUNQEMUPARAMS = "slirp nographic novga"
TEST_SUITES = "\
    date \
    df \
    parselogs \
    ping \
    ptest \
    python \
    rtc \
    ssh \
"

# uses meta-ledge-secure secure boot
IMAGE_INSTALL += "\
    ${@bb.utils.contains("DISTRO_FEATURES", "wifi", "packagegroup-base-wifi", "", d)} \
    ${@bb.utils.contains("MACHINE_FEATURES", "optee", "packagegroup-ledge-optee", "", d)} \
    ${@bb.utils.contains("MACHINE_FEATURES", "tpm2", "packagegroup-ledge-tpm-lava", "", d)} \
    ${@bb.utils.contains("MACHINE_FEATURES", "tpm2", "packagegroup-security-tpm2", "", d)} \
    ${@bb.utils.contains("MACHINE_FEATURES", "tsn", "packagegroup-ledge-tsn", "", d)} \
    efivar \
    fwts \
    fwupd \
    fwupd-efi \
    kernel-modules \
    strace \
    xz \
"

EXTRA_IMAGE_FEATURES += "package-management"

do_deploy() {
    # to customise oeqa tests
    mkdir -p "${DEPLOYDIR}"
    install "${WORKDIR}/testimage_data.json" "${DEPLOYDIR}"
    # workaround meta-ts firmware link for testimage.bbclass
    ( cd "${DEPLOYDIR}" && \
      ln -sf ../../../../tmp_tsqemuarm64-secureboot/deploy/images/tsqemuarm64-secureboot/flash.bin flash.bin \
    )
}
# do_unpack needed to run do_fetch and do_unpack which are disabled by image.bbclass.
addtask deploy before do_build after do_rootfs do_unpack
